<?php 

function gen_url($host, $title) {
  return $host . "/php-test/page.php?page=" .  str_replace(" ", "-", strtolower($title)); 
}

function gen_url_slug($title) {
  return str_replace(" ", "-", strtolower($title)); 
}

function gen_short_url($host) {
  return $host . "/php-test/" .  substr(md5(microtime()),rand(0,26),6); 
}

$host = "http://{$_SERVER['HTTP_HOST']}";
$msg = -1;
if (isset($_POST) && !empty($_POST)) {
  $title = $_POST['page-title'];
  $url = gen_url($host, $title);
  $short_url = gen_short_url($host);
  $url_slug = gen_url_slug($title);
  $sql = "INSERT INTO page (url, short_url, title, url_slug) values('".$url."','".$short_url."','".$title."','".$url_slug."')";

  if ($conn->query($sql) === TRUE) {
    $msg = 1;
  } else {
    $msg = 0;
  }
}

?>

<form method="post" action="create-page.php">
  <div class="form-group">
    <label for="pageTitle">Page Title</label>
    <input type="text" name="page-title" class="form-control" id="pageTitle" aria-describedby="page-title" placeholder="Enter title">
    <small id="page-title" class="form-text text-muted">Enter you page title.</small>
  </div>
  <input type="submit" class="btn btn-primary" value="Create page">
</form>

<?php if($msg == 1): ?>
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-success">
      <p>Successfully inserted the page</p>
      <p>URL : <?php  echo $url; ?></p>
      <p>Short URL : <?php  echo $short_url; ?></p>
      <p>Title : <?php  echo $title; ?></p>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if($msg == 0): ?>
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-danger">
      <p>Error : <?php echo $conn->error; ?></p>
    </div>
  </div>
</div>
<?php endif; ?>
