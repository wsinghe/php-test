<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Shortener URL</h1>
  <p class="lead">Build a small link shortener service with an API as well as a small web front-end..</p>
</div>