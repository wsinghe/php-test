<footer class="pt-4 my-md-5 pt-md-5 border-top">
<div class="row">
  <div class="col-12 col-md">
    <img class="mb-2" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
    <small class="d-block mb-3 text-muted">PHP test &copy; 2017-2018</small>
  </div>
</div>
</footer>