<div class="card-deck mb-3 text-center">
  <div class="card mb-4 box-shadow">
    <div class="card-header">
      <h4 class="my-0 font-weight-normal">Create a page</h4>
    </div>
    <div class="card-body">
      <p>
        After you create page you, It will create a new page, URL and short URL also...
      </p>
      <a href="create-page.php" type="button" class="btn btn-lg btn-block btn-primary">GO</a>
    </div>
  </div>
  <div class="card mb-4 box-shadow">
    <div class="card-header">
      <h4 class="my-0 font-weight-normal">Show Short URL</h4>
    </div>
    <div class="card-body">
      <p>
        You can get the short URL using Long URL...
      </p>
      <a href="request-short-url.php" type="button" class="btn btn-lg btn-block btn-primary">GO</a>
    </div>
  </div>
  <div class="card mb-4 box-shadow">
    <div class="card-header">
      <h4 class="my-0 font-weight-normal">Show Long URL</h4>
    </div>
    <div class="card-body">
      <p>
        You can get the long URL using short URL...
      </p>
      <a href="request-url.php" type="button" class="btn btn-lg btn-block btn-primary">GO</a>
    </div>
  </div>
</div>