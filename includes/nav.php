<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
  <h5 class="my-0 mr-md-auto font-weight-normal"><a href="/php-test">Shortener URL</a></h5>
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark" href="create-page.php">Create a page</a>
    <a class="p-2 text-dark" href="request-short-url.php">Show Short URL</a>
    <a class="p-2 text-dark" href="request-url.php">Show Long URL</a>
  </nav>
</div>