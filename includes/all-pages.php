<?php 

$sql = "SELECT * FROM page";
$result = $conn->query($sql);

?>
<?php if ($result->num_rows > 0) { ?>
<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto">
  <h2 class="display-4">All Pages List</h2>
  <ul>
  	<?php while ($row = mysqli_fetch_array($result)) { 
  		if(!empty($row["title"])) { ?>
  	    	<li><a href="<?php echo $row["url"] ?>"><?php echo $row["title"] ?></a></li>
  	    <?php } ?>
  	<?php } ?>
  </ul>
</div>

<?php } ?>