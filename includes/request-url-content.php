<?php 

$msg = -1;
if (isset($_POST) && !empty($_POST)) {
  $url = $_POST['url'];
  $sql = "SELECT url FROM page WHERE short_url LIKE '%".$url."'";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    $row = mysqli_fetch_array($result);
    $url = $row["url"];
    $msg = 1;
  } else {
    $msg = 0;
  }
}

?>

<form method="post" action="request-url.php">
  <div class="form-group">
    <label for="pageTitle">Short URL</label>
    <input type="text" name="url" class="form-control" id="url" aria-describedby="url" placeholder="Enter URL">
    <small id="url" class="form-text text-muted">Enter you page Short URL.</small>
  </div>
  <input type="submit" class="btn btn-primary" value="SEARCH">
</form>

<?php if($msg == 1): ?>
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-success">
      <strong>Short URL : <?php  echo $url; ?></strong>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if($msg == 0): ?>
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-danger">
      <p>Countn't fine requested URL</p>
    </div>
  <div class="col-sm-12">
</div>
<?php endif; ?>
