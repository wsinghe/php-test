
<?php 

include("php/db_connect.php");
include("php/visited_data.php");

$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

$sql = "SELECT * FROM page WHERE short_url LIKE '%".$actual_link."'";
$result = $conn->query($sql);

$sql1 = "SELECT * FROM visitor WHERE ip_address LIKE '".$ip_address."'";
$result1 = $conn->query($sql1);

if ($result->num_rows > 0) {
  $row = mysqli_fetch_array($result);

  if($result1->num_rows == 0) {
    $sql2 = "INSERT INTO visitor (http_referer, user_agent, ip_address,  ip_value) values('".$http_referer."','".$user_agent."','".$ip_address."','".$ip_value."')";

    if ($conn->query($sql2) === TRUE) {
      $id = mysqli_insert_id($conn);
    }
  } else {
    $row1 = mysqli_fetch_array($result1);
    $id =$row1["visitor_id"];
  }

  $sql3 = "INSERT INTO page_visitor (visitor_id, page_id, date_time) values(".$id.",".$row["page_id"].",".time().")";
    if ($conn->query($sql3) === TRUE) {
      $id = mysqli_insert_id($conn);
    }
  header('Location: '.$row["url"]);
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shortener URL | Sample PHP</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/pricing.css" rel="stylesheet">
  </head>

  <body>

    <?php include("includes/nav.php"); ?>


    <?php include("includes/page-title.php"); ?>

    <div class="container">
      
      <?php include("includes/home.php"); ?>

      <?php include("includes/all-pages.php"); ?>      

      <?php include("includes/footer.php"); ?>

    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  </body>
</html>
