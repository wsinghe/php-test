-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2018 at 09:18 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `url` varchar(500) NOT NULL,
  `short_url` varchar(100) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `url_slug` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `url`, `short_url`, `title`, `url_slug`) VALUES
(8, 'http://localhost:4555/php-test/page.php?page=create-page-0001', 'http://localhost:4555/php-test/3f2939', 'Create page 0001', 'create-page-0001'),
(9, 'http://localhost:4555/php-test/page.php?page=this-a-sample-title-01', 'http://localhost:4555/php-test/955ec9', 'This a sample title 01', 'this-a-sample-title-01'),
(10, 'http://localhost:4555/php-test/page.php?page=this-a-sample-title-02', 'http://localhost:4555/php-test/bbbde6', 'This a sample title 02', 'this-a-sample-title-02'),
(11, 'http://localhost:4555/php-test/page.php?page=this-a-sample-title-03', 'http://localhost:4555/php-test/8f06f6', 'This a sample title 03', 'this-a-sample-title-03'),
(12, 'http://localhost:4555/php-test/page.php?page=this-a-sample-title-04', 'http://localhost:4555/php-test/7241e8', 'This a sample title 04', 'this-a-sample-title-04');

-- --------------------------------------------------------

--
-- Table structure for table `page_visitor`
--

CREATE TABLE `page_visitor` (
  `pv_id` int(11) NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_visitor`
--

INSERT INTO `page_visitor` (`pv_id`, `visitor_id`, `page_id`, `date_time`) VALUES
(1, 4, 8, '0000-00-00 00:00:00'),
(2, 4, 8, '0000-00-00 00:00:00'),
(3, 4, 9, '0000-00-00 00:00:00'),
(4, 4, 9, '0000-00-00 00:00:00'),
(5, 5, 10, '0000-00-00 00:00:00'),
(6, 4, 10, '0000-00-00 00:00:00'),
(7, 4, 10, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `visitor_id` int(6) NOT NULL,
  `http_referer` varchar(250) NOT NULL DEFAULT '',
  `user_agent` varchar(250) NOT NULL DEFAULT '',
  `ip_address` varchar(20) NOT NULL DEFAULT '',
  `ip_value` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`visitor_id`, `http_referer`, `user_agent`, `ip_address`, `ip_value`) VALUES
(4, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '::1', 0),
(5, 'vxvxxcv', 'vxcvxvc', '::2', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `page_visitor`
--
ALTER TABLE `page_visitor`
  ADD PRIMARY KEY (`pv_id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`visitor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `page_visitor`
--
ALTER TABLE `page_visitor`
  MODIFY `pv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `visitor_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
